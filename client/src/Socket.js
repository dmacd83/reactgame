import io from 'socket.io-client';


class Socket {
    constructor(url) {
        this.socket = io(url);
    }

    on (name, callback) {
        this.socket.on(name, callback);
    }

    off (name) {
        this.socket.off(name);
    }

    once (name, callback) {
        this.socket.once(name, callback);
    }

    emit (name, data) {
        this.socket.emit(name, data);
    }
}


export default Socket;
