import React from 'react';
import Config from './AppConfig.js';
import Socket from './Socket.js';
import AppMain from './Components/AppMain.js';
import './App.css';


const App = () => {
    let socket = new Socket(Config.webSocketUrl);

    return (
      <AppMain socket={socket} />
    )
}


export default App;
