let _ = require('lodash');
let png = require('png-js');


let users = {}, online = {}, timer = null, image = [];
let mapSize = 1024, map = {}, spawn = null;
let updateInterval = 50;
let tileTypes = {
    [255 * 65536 + 255 * 256 + 0]:   { name: 'spawn', url: 'spawn.png', char: '*' },
    [127 * 65536 + 127 * 256 + 0]:   { name: 'ground', url: 'ground.png', char: '.' },
    [200 * 65536 + 100 * 256 + 255]: { name: 'wall', url: 'wall.png', char: '#', blocking: true },
    [50  * 65536 + 100 * 256 + 255]: { name: 'wall2', url: 'wall2.png', char: 'X', blocking: true }
};

/*
    map
    ---
    'world.png' must be same size as `mapSize`

    pixel format:
    R * 65536 + G * 256 + B = Tile Type
    A = Unused

    tile types:
    - 255,255,0 = Spawn (65535, 0)
    - 127,127,0 = 'default' ground non-blocking
    - 200,100,255 = 'default' wall blocking
    - 50,100,255 = wall blocking

    for ease of editing always choose tile types that are sufficiently distant from other tile types
    as this makes it easier to read the map in the image editor if human-editing

    
*/


const init = () => {
    console.log('game.init: loading world...');
    png.decode('world.png', pixels => {
        //RGBA
        image = pixels;
        //make sparse map from PNG image
        for (let idx = 0, x = 0, y = 0; idx < pixels.length; idx += 4) {
            let r = pixels[idx], g = pixels[idx+1], b = pixels[idx+2];
            if (r > 0) {
                let location = locationGenerate(x, y, r, g, b);
                if (_.isNil(map[y])) map[y] = {};
                map[y][x] = location;
                if (location.type === 255 * 65536 + 255 * 256 + 0) {
                    spawn = location;
                }
                console.log(`game.init: created location ${x},${y}`);
            }
            if (++x % mapSize == 0) {
                x = 0;
                y++;
            }
        }

        if (_.isNil(spawn)) {
            console.log('game.init: no spawn found! (pixel colour R255,G255,B0)');
            process.exit(0);
        }

        console.log('game.init: finished loading world.');
        timer = setInterval(update, updateInterval);
    });
};


const locationGenerate = (x, y, r, g, b) => {
    let loc = {
        x: x,
        y: y,
        type: r * 65536 + g * 256 + b
    };
    return loc;
};


const exit = () => {
    Object.keys(online).map(username => {
        online[username].socket = null;
        delete online[username];
    });

    if (!_.isNil(timer)) clearInterval(timer);
};


const update = () => {
    Object.values(online).map(u => {
        if (!u.needsUpdate) return;
        let localmap = [];
        for (let y = u.position.y - 20; y <= u.position.y + 20; y++) {
            if (_.isNil(map[y])) continue;
            for (let x = u.position.x - 40; x <= u.position.x + 40; x++) {
                if (_.isNil(map[y][x])) continue;
                localmap.push(map[y][x]);
            }
        }
        u.socket.emit('update', {
            position: u.position,
            mapSize: mapSize,
            map: localmap
        });
        u.needsUpdate = false;
    });
};


const userCreate = (username, password) => {
    if (userExists(username)) return null;
    let user = {
        username: username,
        password: password,
        socket: null,
        position: {
            x: spawn.x,
            y: spawn.y
        },
        location: map[spawn.y][spawn.x],
        needsUpdate: false
    };
    users[username] = user;
    return user;
};


const userExists = (username) => {
    return !_.isNil(_.get(users, username, null));
};


const userGet = (username) => {
    return _.get(users, username, null);
};


const userGetBySocketId = (id) => {
    let user = Object.values(online).filter(u => !_.isNil(u.socket) && u.socket.id === id);
    if (user.length > 0) return user[0];
    return null;
}


const userAuthenticate = (username, password) => {
    let user = _.get(users, username, null);
    return !_.isNil(user) && _.get(user, 'password', null) === password;
};


const userOnline = (username, socket) => {
    let user = userGet(username);
    if (_.isNil(user)) return false;
    user.socket = socket;
    setTimeout(() => {
        user.socket.emit('map-info', {
            size: mapSize,
            types: tileTypes
        });
        user.needsUpdate = true;
    }, 2000);
    online[username] = user;
    return true;
};


const userOffline = (username) => {
    let user = userGet(username);
    if (_.isNil(user)) return false;
    user.socket = null;
    delete online[username];
    return true;
};


const userOfflineBySocketId = (id) => {
    let user = userGetBySocketId(id);
    if (_.isNil(user)) return false;
    return userOffline(user.username);
};


const chat = (user, message, recipient = '') => {
    console.log(`<${user.username} => ${recipient}> ${message}`);
    if (recipient == '') {
        Object.values(online).map(u => !_.isNil(u.socket) && u.socket.emit('user-chat', { username: user.username, msg: message, pm: false }));
    } else {
        let recip = userGet(recipient);
        if (_.isNil(recip) || _.isNil(recip.socket)) {
            user.socket.emit('user-chat-failed', { reason: 'recipient is not online or doesnt exist' });
            return false;
        }
        recip.socket.emit('user-chat', { username: user.username, msg: message, pm: true });
    }
    return true;
};

const locationGet = (x, y) => {
    if (_.isNil(map[y]) || _.isNil(map[y][x])) return null;
    return map[y][x];
};

const move = (user, direction) => {
    let loc = null, type = null;
    switch (direction) {
        case 'north':
            loc = locationGet(user.position.x, user.position.y - 1);
            type = _.get(tileTypes, loc.type, null);
            if (_.isNil(loc) || _.isNil(type) || type.blocking) break;
            user.location = loc;
            user.position.y--;
            user.needsUpdate = true;
            break;
        case 'south':
            loc = locationGet(user.position.x, user.position.y + 1);
            type = _.get(tileTypes, loc.type, null);
            if (_.isNil(loc) || _.isNil(type) || type.blocking) break;
            user.location = loc;
            user.position.y++;
            user.needsUpdate = true;
            break;
        case 'west':
            loc = locationGet(user.position.x - 1, user.position.y);
            type = _.get(tileTypes, loc.type, null);
            if (_.isNil(loc) || _.isNil(type) || type.blocking) break;
            user.location = loc;
            user.position.x--;
            user.needsUpdate = true;
            break;
        case 'east':
            loc = locationGet(user.position.x + 1, user.position.y);
            type = _.get(tileTypes, loc.type, null);
            if (_.isNil(loc) || _.isNil(type) || type.blocking) break;
            user.location = loc;
            user.position.x++;
            user.needsUpdate = true;
            break;
        case 'northwest':
            loc = locationGet(user.position.x - 1, user.position.y - 1);
            type = _.get(tileTypes, loc.type, null);
            if (_.isNil(loc) || _.isNil(type) || type.blocking) break;
            user.location = loc;
            user.position.x--;
            user.position.y--;
            user.needsUpdate = true;
            break;
        case 'northeast':
            loc = locationGet(user.position.x + 1, user.position.y - 1);
            type = _.get(tileTypes, loc.type, null);
            if (_.isNil(loc) || _.isNil(type) || type.blocking) break;
            user.location = loc;
            user.position.x++;
            user.position.y--;
            user.needsUpdate = true;
            break;
        case 'southwest':
            loc = locationGet(user.position.x - 1, user.position.y + 1);
            type = _.get(tileTypes, loc.type, null);
            if (_.isNil(loc) || _.isNil(type) || type.blocking) break;
            user.location = loc;
            user.position.x--;
            user.position.y++;
            user.needsUpdate = true;
            break;
        case 'southeast':
            loc = locationGet(user.position.x + 1, user.position.y + 1);
            type = _.get(tileTypes, loc.type, null);
            if (_.isNil(loc) || _.isNil(type) || type.blocking) break;
            user.location = loc;
            user.position.x++;
            user.position.y++;
            user.needsUpdate = true;
            break;
        default:
            break;
    }
};


module.exports = {
    init,
    exit, 
    userCreate,
    userExists,
    userGet,
    userGetBySocketId,
    userAuthenticate,
    userOnline,
    userOffline,
    userOfflineBySocketId,
    chat,
    move,
    users,
    online
};
