import React, { Component } from 'react';
import _ from 'lodash';


class Item extends Component {
    constructor () {
        super();
        this.state = {
            isOpen: false
        };
    }

    toggleDisplay = () => {
        this.setState({ isOpen: !_.get(this, 'state.isOpen', false) });
    }

    render () {
        if (_.get(this, 'state.isOpen', false)) {
            return (
                <div>
                    <li onClick={this.toggleDisplay}><strong>{this.props.title}</strong>
                        <ul>
                            <li><strong>UserID:</strong> {this.props.userId}</li>
                            <li><strong>Body:</strong> {this.props.body}</li>
                        </ul>
                    </li>
                </div>
            )
        }

        return (
            <li onClick={this.toggleDisplay}>{this.props.title}</li>
        )
    }
}


export default Item;
