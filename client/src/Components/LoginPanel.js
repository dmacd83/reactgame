import React, { Component } from 'react';

class LoginPanel extends Component {
    submitHandler (event) {
        event.preventDefault();
        let username = event.target.username.value;
        let password = event.target.password.value;
        let create = event.target.create.checked;
        console.log(`LoginPanel.submitHandler: ${username} <password> ${create}`);

        //callback to AppMain to emit the socket event
        this.props.onSubmit(username, password, create);
    }

    render () {
        return (
            <div>
                <form onSubmit={this.submitHandler.bind(this)}>
                    Username: <input type="input" name="username" /><br/>
                    Password: <input type="password" name="password" /><br/>
                    <input type="checkbox" name="create" value="create" />Create New Account?<br/>
                    <input type="submit" name="login" value="Login" />
                </form>
            </div>
        )
    }
}

export default LoginPanel;
