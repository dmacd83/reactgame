import React, { Component } from 'react';
import _ from 'lodash';


class GameInput extends Component {
    constructor () {
        super();
        this.state = {
            loaded: false,
            direction: {x:0,y:0}
        };
    }

    componentDidMount () {
        if (this.state.loaded) return;

        window.onkeydown = e => {
            let direction = null;
            switch(e.which) {
                case 87: //w
                    direction = { 
                        x: this.state.direction.x, 
                        y: -1
                    };
                    break;
                case 83: //S
                    direction = { 
                        x: this.state.direction.x, 
                        y: 1
                    };
                    break;
                case 65: //A
                    direction = { 
                        x: -1,
                        y: this.state.direction.y
                    };
                    break;
                case 68: //D
                    direction = { 
                        x: 1,
                        y: this.state.direction.y
                    };
                    break;
                default:
                    console.log(e.which);
                    break;
            }

            if (!_.isNil(direction)) {
                this.setState({
                    ...this.state,
                    direction
                });
            }
        };

        window.onkeyup = e => {
            let direction = null;
            switch(e.which) {
                case 87: //w
                    direction = { 
                        x: this.state.direction.x, 
                        y: 0
                    };
                    break;
                case 83: //S
                    direction = { 
                        x: this.state.direction.x, 
                        y: 0
                    };
                    break;
                case 65: //A
                    direction = { 
                        x: 0,
                        y: this.state.direction.y
                    };
                    break;
                case 68: //D
                    direction = { 
                        x: 0,
                        y: this.state.direction.y
                    };
                    break;
                default:
                    break;
            }

            if (!_.isNil(direction)) {
                this.setState({
                    ...this.state,
                    direction
                });
            }
        };

        this.setState({
            ...this.state,
            loaded: true
        }, () => {
            setInterval(() => {
                if (this.state.direction.x !== 0 || this.state.direction.y !== 0) {
                    let direction = '';
                    if (this.state.direction.x === -1) {
                        if (this.state.direction.y === -1) direction = 'northwest';
                        else if (this.state.direction.y === 1) direction = 'southwest';
                        else direction = 'west';
                    } else if (this.state.direction.x === 1) {
                        if (this.state.direction.y === -1) direction = 'northeast';
                        else if (this.state.direction.y === 1) direction = 'southeast';
                        else direction = 'east';
                    } else if (this.state.direction.y === -1) {
                        direction = 'north';
                    } else if (this.state.direction.y === 1) {
                        direction = 'south';
                    }
                    this.props.socket.emit('move', { direction });
                }
            }, 50);
        });
    }

    render () {
        return (
            <div>
            </div>
        )
    }
}

export default GameInput;
