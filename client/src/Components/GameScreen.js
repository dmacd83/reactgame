import React, { Component } from 'react';
import _ from 'lodash';
import GameInput from './GameInput.js';


class GameScreen extends Component {
    constructor () {
        super();
        this.state = {
            position: { x: 0, y: 0 },
            mapSize: 1024,
            map: {},
            loaded: false
        };
    }

    componentDidMount () {
        this.props.socket.off('map-info');
        this.props.socket.on('map-info', (data) => {
            console.log(data);
            this.setState({
                ...this.state,
                mapSize: data.size,
                tiles: data.types,
                loaded: true
            }, () => {
                this.props.socket.off('update');
                this.props.socket.on('update', this.onUpdate.bind(this));
            });
        });
    }

    onUpdate (data) {
        let map = this.state.map;
        data.map.map(loc => {
            if (_.isNil(map[loc.y])) map[loc.y] = {};
            map[loc.y][loc.x] = loc;
            return loc;
        });

        this.setState({
            ...this.state,
            position: data.position,
            mapSize: _.isNil(data.mapSize) ? this.state.mapSize : data.mapSize,
            map: map
        }, () => {
            console.log(this.state);
        });
    }

    render () {
        if (!this.state.loaded) {
            return (
                <div>loading...</div>
            )
        }

        let view = [], pos = this.state.position;
        for (let y = pos.y - 20; y <= pos.y + 20; y++) {
            for (let x = pos.x - 40; x <= pos.x + 40; x++) {
                if (x < 0 || x >= this.state.mapSize || y < 0 || y >= this.state.mapSize) {
                    view.push(' '); //out of bounds
                } else {
                    if (!_.isNil(this.state.map[y]) && !_.isNil(this.state.map[y][x])) {
                        let tileId = _.get(this.state, `map.${y}.${x}.type`, null);
                        if (_.isNil(tileId)) {
                            view.push('?');
                            continue;
                        }
                        let tileChar = _.get(this.state, `tiles.${tileId}.char`, ' ');
                        view.push(tileChar);
                    } else {
                        view.push(' ');
                    }
                }
            }
            view.push('\r\n');
        }

        return (
            <div>
                <GameInput socket={this.props.socket} />
                <pre>
                    {view}
                </pre>
            </div>
        )
    }
}

export default GameScreen;
