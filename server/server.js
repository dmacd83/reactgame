let process = require('process');
let express = require('express');
let app = express();
let http = require('http').Server(app);
let io = require('socket.io')(http);
let _ = require('lodash');
let api = require('./http-api.js');
let game = require('./game.js');


let httpPort = 3005, apiRoot = 'api', sockets = {};

/*
    from client:
    - user-create           { username, password }
    - user-login            { username, password }
    - user-logout           { }
    - user-chat             { msg, to = '' }
    - move                  { direction }

    from server:
    - user-create-fail      { reason }
    - user-create-success   { username }
    - user-login-fail       { reason }
    - user-login-success    { reason }
    - user-chat             { username, msg, pm? }
    - user-chat-failed      { reason }
    - update                { map }
*/

const init = () => {
    process.on('beforeExit', exit);
    process.on('SIGINT', exit);
    process.on('SIGTERM', exit);

    game.init();

    app.use(express.static([ __dirname, 'public' ].join('/')));
    app.get(`/${apiRoot}/:func/:args`, apiHandler);
    http.listen(httpPort, () => {
        console.log('init: http listening');
        io.on('connection', onSocketConnect);
    });
};


const exit = () => {
    console.log('exit');
    game.exit();
    Object.values(sockets)
        .filter(socket => !_.isNil(socket))
        .map(socket => socket.disconnect(true));
    io.close();
    process.exit(0);
};


const apiHandler = (req, res) => {
    let func = req.params.func;
    let args = req.params.args ? req.params.args.split(',') : [];
    let fn = _.get(api, func, null);
    if (_.isFunction(fn)) {
        console.log(`http-api: calling ${func}(${args})`);
        fn(req, res, args);
    } else {
        console.log(`http-api: invalid api call '${func}'`);
        res.status(500).send('Invalid API Call Requested');
    }
};


const onSocketConnect = (socket) => {
    console.log(`onSocketConnect: ${socket.id}`);
    sockets[socket.id] = socket;
    socket.on('disconnect', onSocketDisconnect.bind(this, socket));
    socket.on('user-create', onSocketUserCreate.bind(this, socket));
    socket.on('user-login', onSocketUserLogin.bind(this, socket));
    socket.on('user-logout', onSocketUserLogout.bind(this, socket));
    socket.on('user-chat', onSocketUserChat.bind(this, socket));
    socket.on('move', onSocketMove.bind(this, socket));
};


const onSocketDisconnect = (socket) => {
    if (_.isNil(socket) || _.isNil(_.get(socket, 'id', null))) {
        //socket has already been destroyed, nothing to do here
        return;
    }
    let id = socket.id;
    console.log(`onSocketDisconnect: ${id}`);
    game.userOffline(game.userGetBySocketId(id));
    socket.disconnect(true);
    delete sockets[id];
};

const onSocketUserCreate = (socket, data) => {
    if (data.username.length < 3 || data.username.length > 16) {
        socket.emit('user-create-fail', { reason: 'username must be between 3 and 16 characters in length' });
        console.log('onSocketUserCreate: invalid username length');
        return;
    }

    if (game.userExists(data.username)) {
        socket.emit('user-create-fail', { reason: 'username taken' });
        console.log('onSocketUserCreate: username taken');
        return;
    }

    if (data.password.length < 4) {
        socket.emit('user-create-fail', { reason: 'password must be atleast 4 characters in length' }); //TODO: change to sensible value
        console.log('onSocketUserCreate: invalid password length');
        return;
    }

    user = game.userCreate(data.username, data.password);

    console.log(`onSocketUserCreate: created user '${user.username}'`);
    socket.emit('user-create-success', { username: user.username });
};


const onSocketUserLogin = (socket, data) => {
    let user = game.userGetBySocketId(socket.id);
    if (!_.isNil(user)) return; //already logged in
    if (!game.userAuthenticate(data.username, data.password)) {
        socket.emit('user-login-fail', { reason: 'invalid password' });
        console.log(`onSocketUserLogin: invalid password for user '${data.username}'`);
        return;
    }
    user = game.userGet(data.username);
    socket.emit('user-login-success', { username: user.username });
    game.userOnline(data.username, socket);
    console.log(`onSocketUserLogin: ${user.username}`);
};


const onSocketUserLogout = (socket, data) => {
    game.userOfflineBySocketId(socket.id);
};


const onSocketUserChat = (socket, data) => {
    let user = game.userGetBySocketId(socket.id);
    if (_.isNil(user)) return;
    game.chat(user, _.get(data, 'msg', '???'), _.get(data, 'to', ''));
};

const onSocketMove = (socket, data) => {
    let user = game.userGetBySocketId(socket.id);
    let direction = _.get(data, 'direction', null);
    if (_.isNil(user) || _.isNil(direction)) return;
    game.move(user, direction);
};


init();


module.exports = {
    init: init,
    exit: exit,
    apiHandler: apiHandler,
    io: io
};