import React, { Component } from 'react';
import LoginPanel from './LoginPanel.js';
import GameScreen from './GameScreen.js';
import _ from 'lodash';

class AppMain extends Component {
    constructor () {
        super();
        this.state = { 
            authenticated: false,
            username: '',
            password: '',
            chat: [], 
            count: 0
        };
    }

    onLogin (username, password, create = false) {
        console.log(`AppMain.onLogin: ${username} <password> ${create}`);

        this.props.socket.off('user-login-success');
        this.props.socket.off('user-create-success');
        this.props.socket.off('user-chat');

        this.setState({
            ...this.state, 
            username: username, 
            password: password
        }, () => {
            this.props.socket.on('user-login-success', (data) => {
                console.log('user-login-success');
                this.setState({
                    ...this.state,
                    authenticated: true
                }, this.onAuthenticated);
            });

            //subscribe to the user-create-success event only once state has updated
            this.props.socket.on('user-create-success', (data) => {
                //attempt to login with freshly created user
                this.props.socket.emit('user-login', {
                    username: this.state.username,
                    password: this.state.password
                });
                console.log('user-create-success');
            });
            //fire the user-(create|login) event
            this.props.socket.emit(`user-${(create ? 'create' : 'login')}`, {
                username: username,
                password: password
            });
        });
    }

    onAuthenticated () {

        
        this.props.socket.on('user-chat', (data) => {
            let chat = [...this.state.chat, data ];
            this.setState({ 
                ...this.state, 
                chat: chat, 
                count: this.state.count + 1 //TODO: remove me, this counter is only used in testing
            });
        });

        //TODO: remove me -- this is only for testing
        setInterval(() => {
            if (this.state.authenticated) {
                this.props.socket.emit('user-chat', { msg: `test ${this.state.count}`, to: '' });
            }
        }, 1000);
    }

    render () {
        if (this.state === undefined) return;
        if (!this.state.authenticated) {
            return (
                <LoginPanel onSubmit={this.onLogin.bind(this)} />
            )
        }

        return (
            <GameScreen socket={this.props.socket} />
        )

        /*
        let chat = this.state.chat.map((c,i) => <li key={i}>[{c.username}] {c.msg}</li>);
        return (
            <div>{chat}</div>
        )*/
    }
}

export default AppMain;
