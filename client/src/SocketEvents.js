let events = {
    connect: (socket, data) => {
        console.log('connected');
        socket.emit('user-chat', { msg: 'testing...' });
        this.setState({
            socket: socket,
            connected: true
        });
    },
    disconnect: (socket, data) => {
        console.log('disconnected');
    }
};

export default events
