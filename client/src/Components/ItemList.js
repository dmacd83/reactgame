import React, { Component } from 'react';
import Item from './Item.js';
import $ from 'jquery';
import _ from 'lodash';

class ItemList extends Component {
    constructor () {
        super();
        this.state = { items: [] };
    }

    getItemData (url) {
        $.ajax({
            url: url,
            dataType: 'json',
            success: ((data) => {
                let items = data.map((itemData, i) => {
                    return <Item 
                        key={i} 
                        userId={itemData.userId} 
                        title={itemData.title} 
                        body={itemData.body}
                    />
                });
                this.setState({
                    items: items
                }, () => console.log('getItemData complete'));
            })
        });
    }

    componentDidMount () {
        this.getItemData(this.props.url);
    }

    render () {
        if (!_.get(this, 'state.items.length', 0)) {
            return (
                <div id='itemList'>
                    Loading...
                </div>
            )
        }

        return (
            <div id='itemList'>
                <h3>Items</h3>
                {this.state.items}
            </div>
        )
    }
}


export default ItemList;
